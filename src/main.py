import tensorflow as tf
import numpy as np
import preprocess as preprocess
import network as network
import util as util
import argparse
import datetime

parser = argparse.ArgumentParser()
parser.add_argument("--load", help="load weights", action="store_true")
parser.add_argument("--train", help="train joint model", action="store_true")
parser.add_argument("--rate", help="learning step", type=float)
parser.add_argument("--test_percent", help="percent test data", type=float)
parser.add_argument("--steps", help="learning step", type=int)
parser.add_argument("--gen", help="gen data", action="store_true")

args = parser.parse_args()


if __name__ == '__main__':

    rate = 0.0001 if not args.rate else args.rate
    steps = 20 if not args.steps else args.steps
    percent_test = 0.1 if not args.test_percent else args.test_percent

    savefile = "inv5x5_15_sep_bias"
    data, lbs = preprocess.load_batches("inv5x5_6_6_15")
    # indices = preprocess.compute_active_indices(6, 6);
    # data = _data[-120:, ...]
    # lbs = _lbs[-120:, ...]
    print("total shape", data.shape, lbs.shape)

    test_indices, train_indices = util.pick_one_every(int(1 / percent_test), data.shape[0])

    test_data = data[test_indices, ...]
    test_lbs = lbs[test_indices, ...]
    print("test stat", np.min(test_lbs), np.max(test_lbs))

    train_data = data[train_indices, ...]
    train_lbs = lbs[train_indices, ...]

    print("test shape", test_data.shape, test_lbs.shape)
    print("train shape", train_data.shape, train_lbs.shape)

    with tf.Session() as sess:
        net = network.Network(sess, rate, data.shape[1:5])
        sess.run(tf.global_variables_initializer())

        if args.load:
            try:
                net.load("./artifacts/" + savefile)
            except Exception:
                print(">>>> The model is not loaded.")

        if args.train:
            net.train(train_data, train_lbs, 30, "./artifacts/" + savefile, steps)

        how_much_cost, rain_or_not_cost, lmsq, truepos, falsepos = net.eval(test_data, test_lbs)
        print("final unaverage cost on test set (how much, rain or not, lmsq, truepos, falsepos):", how_much_cost, rain_or_not_cost, lmsq, truepos, falsepos)

        if args.gen:
            sep_lbs = preprocess.get_label_batches(datetime.datetime(2017, 9, 1, 0, 0), datetime.datetime(2017, 10, 1, 0, 0), batch_length_in_hours=6, leading_hours=6)
            preprocess.gen_result_file(sep_lbs, datetime.datetime(2017, 9, 1, 0, 0), datetime.datetime(2017, 10, 1, 0, 0), batch_length_in_hours=6, leading_hours=6, writeflag='w')
            gen = net.run(_data)
            preprocess.gen_result_file(gen, datetime.datetime(2017, 10, 1, 0, 0), datetime.datetime(2017, 11, 1, 0, 0), batch_length_in_hours=6, leading_hours=6, writeflag='a')
