import math
import numpy as np
import datetime


def check_and_update_list(list, tuple, key=None):
    if key is not None:
        indices = [index for index, item in enumerate(list) if item[key] == tuple]
        if len(indices) > 0:
            return indices[0]
        else:
            list.append(tuple)
            return len(list) - 1
    else:
        indices = [index for index, item in enumerate(list) if item == tuple]
        if len(indices) > 0:
            return indices[0]
        else:
            list.append(tuple)
            return len(list) - 1


def datetime_iterate(dt, add_mins):
    return dt + datetime.timedelta(0, add_mins * 60)


def pick_one_every(k, total):
    chosen = np.arange(k, total, k)
    rejected = np.setdiff1d(np.arange(0, total), chosen)
    return chosen, rejected


if __name__ == '__main__':
    print(datetime_iterate(datetime.datetime(2017, 5, 1, 2, 30), -3600))
    print(pick_one_every(5, 100))
