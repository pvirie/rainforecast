import tensorflow as tf
import numpy as np
import preprocess as preprocess
import cell as cell


def linear_layer(x, w, b):
    """ (..., input_size) * (input_size, output_size) -> (..., output_size) """
    return tf.reshape(tf.matmul(tf.reshape(x, [-1, tf.shape(w)[0]]), w), tf.concat([tf.shape(x)[:-1], [tf.shape(w)[1]]], 0)) + b


def conv1d_layer(x, w, b):
    s = tf.shape(x)
    flat = tf.reshape(x, [-1, s[2], s[3]])
    o = tf.nn.conv1d(flat, w, 1, 'SAME') + b
    return tf.reshape(o, tf.concat([tf.shape(x)[:-1], [tf.shape(w)[2]]], 0))


class Network:

    def __init__(self, session, learning_rate, data_size, rnn_size=16):
        self.sess = session
        self.data_size = data_size
        self.rnn_size = rnn_size
        self.datum_size = self.data_size[2] * self.data_size[3]

        self.gpu_inputs = tf.placeholder(tf.float32, [None, data_size[0], data_size[1], data_size[2], data_size[3]])
        self.gpu_labels = tf.placeholder(tf.float32, [None, data_size[0], data_size[1]])

        self.Wi = tf.Variable((np.random.rand(30, self.datum_size, self.rnn_size) - 0.5) * 0.01, dtype=tf.float32)
        self.bi = tf.Variable(np.zeros((self.rnn_size)), dtype=tf.float32)

        flat = tf.reshape(self.gpu_inputs, [-1, data_size[0], data_size[1], self.datum_size])
        self.rnn_inputs = tf.nn.elu(conv1d_layer(flat, self.Wi, self.bi))

        with tf.variable_scope("lstm"):
            self.rnn = cell.BlockConvolutionCell((self.data_size[1], self.rnn_size))
            state = self.rnn.init_state(tf.shape(self.rnn_inputs)[0])

        outputs = []
        for i in range(data_size[0]):
            output, state = self.rnn(self.rnn_inputs[:, i, ...], state)
            outputs.append(output)

        stacked = tf.stack(outputs, axis=1)
        self.W = tf.Variable((np.random.rand(self.rnn_size, 3) - 0.5) * 0.01, dtype=tf.float32)
        self.b = tf.Variable(np.zeros((3)), dtype=tf.float32)
        size = tf.shape(self.gpu_inputs)[0:3]
        preout = linear_layer(stacked, self.W, self.b)
        how_much = tf.nn.elu(preout[:, :, :, 0])
        rain_or_not = tf.nn.softmax(preout[:, :, :, 1:3], 3)

        ones = tf.ones(size, dtype=tf.float32)
        zeros = tf.zeros(size, dtype=tf.float32)

        sure_rain = rain_or_not[:, :, :, 0]
        sure_clear = rain_or_not[:, :, :, 1]
        self.y = sure_rain * how_much

        mask = tf.where(tf.greater_equal(self.gpu_labels, 0), ones, zeros)
        mask_rain = tf.where(tf.greater(self.gpu_labels, 0.1), ones, zeros)

        self.rain_or_not_cost = -tf.reduce_sum(mask * (mask_rain * tf.log(sure_rain + 1e-8) + (1 - mask_rain) * tf.log(sure_clear + 1e-8))) / tf.reduce_sum(mask)
        self.how_much_cost = tf.reduce_sum(mask_rain * tf.squared_difference(self.y, self.gpu_labels)) / tf.reduce_sum(mask_rain)
        self.overall_cost = self.how_much_cost + 1000 * self.rain_or_not_cost

        self.lmsq = tf.sqrt(tf.reduce_sum(mask * tf.squared_difference(self.y, self.gpu_labels)) / tf.reduce_sum(mask))
        is_rain = tf.where(tf.greater(sure_rain, sure_clear), ones, zeros)
        self.truepos = tf.reduce_sum(mask_rain * is_rain) / tf.reduce_sum(mask_rain)
        self.falsepos = tf.reduce_sum((1 - mask_rain) * is_rain) / tf.reduce_sum(1 - mask_rain)

        scope = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
        self.training_op = tf.train.AdamOptimizer(learning_rate).minimize(self.overall_cost, var_list=scope)
        self.saver = tf.train.Saver(var_list=scope, keep_checkpoint_every_n_hours=1)

    def train(self, batches, lbs, size, session_name, max_iteration=10):

        indices = np.arange(batches.shape[0])
        np.random.shuffle(indices)

        if batches.shape[0] % size > 0:
            total_batches = batches.shape[0] // size + 1
        else:
            total_batches = batches.shape[0] // size

        messages = ""
        for step in range(max_iteration):
            sum_loss = 0.0
            for b in range(total_batches):
                db = batches[indices[b * size: (b + 1) * size], ...]
                lb = lbs[indices[b * size: (b + 1) * size], ...]
                _, loss = self.sess.run((self.training_op, self.overall_cost), feed_dict={self.gpu_inputs: db, self.gpu_labels: lb})
                sum_loss += loss

            messages = messages + str(step) + ", " + str(sum_loss / total_batches) + "\n"
            print(step, sum_loss / total_batches)

            if step % 10 == 0:
                self.saver.save(self.sess, session_name)
                with open("./log.txt", "a") as myfile:
                    myfile.write(messages)
                messages = ""

        self.saver.save(self.sess, session_name)

    def load(self, session_name):
        print("loading from last save...")
        self.saver.restore(self.sess, session_name)

    def run(self, batches):
        classes = np.zeros((batches.shape[0], batches.shape[1], batches.shape[2]), dtype=np.float32)
        for i in range(batches.shape[0]):
            classes[i:i + 1, ...] = self.sess.run(self.y, feed_dict={self.gpu_inputs: batches[i:i + 1, ...]})
        return classes

    def eval(self, batches, lbs):
        cost = self.sess.run((self.how_much_cost, self.rain_or_not_cost, self.lmsq, self.truepos, self.falsepos), feed_dict={self.gpu_inputs: batches, self.gpu_labels: lbs})
        return cost


if __name__ == '__main__':

    data, lbs = preprocess.load_batches()
    with tf.Session() as sess:
        net = Network(sess, 0.001, data.shape[1:5])
        sess.run(tf.global_variables_initializer())
        net.train(data, lbs, 10, "./artifacts/test")
