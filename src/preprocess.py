import netCDF4 as nc4
import cv2
import numpy as np
import os
import datetime
import csv
import bootstrap_result as labels
import util as util
import argparse


irband = '13'
dir_path = "/data/irdata/ir" + irband + "nc/"


def nc4_to_nparray(f):
    as_img = f.variables['tbb' + irband][:]
    # minof = np.min(as_img)
    # maxof = np.max(as_img)
    # print(">>>", minof, maxof)
    minof = 150
    maxof = 250
    return (as_img - minof) / (maxof - minof)

# return (stations, r, c)


def sample_atmosphere(irimg, station_locs, size):
    wx = (size[0] - 1) // 2
    wy = (size[1] - 1) // 2
    img = np.zeros((station_locs.shape[0], size[0], size[1]), dtype=np.float32)
    for i in range(station_locs.shape[0]):
        # img[i, ...] = cv2.getRectSubPix(irimg, size, (station_locs[i][1], station_locs[i][0]))
        img[i, ...] = irimg[(station_locs[i][0] - wx):(station_locs[i][0] + wx + 1), (station_locs[i][1] - wy):(station_locs[i][1] + wy + 1)]
    img = np.where(img < 0, 2.0, img)
    return img


# not include to_datetime
def get_data(from_datetime, to_datetime, size=(5, 5)):

    stations = np.asarray(labels.stations, dtype=np.float32)
    dt = from_datetime
    totals = int((to_datetime - from_datetime).total_seconds() // 600)

    data = np.ones((totals, stations.shape[0], size[0], size[1]), dtype=np.float32) * 2.0
    for i in range(totals):
        filename = "IR" + irband + "_" + dt.strftime("%Y%m%d_%H%M") + ".nc"
        if os.path.isfile(dir_path + filename):
            f = nc4.Dataset(dir_path + filename, 'r')

            if 'tbb' + irband in f.variables:
                lats = len(f.variables['latitude']) - np.searchsorted(f.variables['latitude'][::-1], stations[:, 0], side="right")
                longs = np.searchsorted(f.variables['longitude'], stations[:, 1])
                locs = np.stack([lats, longs], axis=1)

                # read file may corrupted, opencv return -1 for float
                plane = nc4_to_nparray(f)
                data[i, ...] = sample_atmosphere(plane, locs, size)

        dt = util.datetime_iterate(dt, 10)

    return data


# not include to_datetime
def get_labels(from_datetime, to_datetime):

    dt = from_datetime
    totals = int((to_datetime - from_datetime).total_seconds() // 3600)

    ls = np.zeros((totals, len(labels.stations)), dtype=np.float32)
    for i in range(totals):
        filename = "b_" + dt.strftime("%Y%m%d_%H%M") + ".nc"
        if os.path.isfile(labels.dir_path + filename):
            ls[i, ...] = np.loadtxt(labels.dir_path + filename, dtype=np.float32)
        dt = util.datetime_iterate(dt, 60)

    return ls


def compute_active_indices(leading_hours=2, batch_length_in_hours=6):
    return np.arange(leading_hours * 6, (leading_hours + batch_length_in_hours) * 6, 6)


def gen_result_file(predict_lbs, from_datetime, to_datetime, leading_hours=2, batch_length_in_hours=6, writeflag='w'):
    out = np.reshape(predict_lbs[:, compute_active_indices(leading_hours, batch_length_in_hours), :], (-1, len(labels.stations)))
    with open(os.path.dirname(os.path.realpath(__file__)) + "/../data/" + 'output.csv', writeflag, newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        totals = int((((to_datetime - from_datetime).total_seconds() // 3600) // batch_length_in_hours) * batch_length_in_hours)
        dt = from_datetime
        for i in range(totals):
            # 2017-05-01 00:00:00,18.8065278,97.907694,0.0
            for j, locs in enumerate(labels.stations):
                writer.writerow([dt.strftime('%Y-%m-%d %H:%M:%S'), locs[0], locs[1], out[i, j]])
            dt = util.datetime_iterate(dt, 60)


def get_data_batches(from_datetime, to_datetime, leading_hours=2, batch_length_in_hours=6, area_size=(5, 5)):

    print("irband", irband)

    all_data = get_data(util.datetime_iterate(from_datetime, -60 * leading_hours), to_datetime, area_size)

    label_size_0 = int((to_datetime - from_datetime).total_seconds() // 3600) // batch_length_in_hours
    data_size_1 = (batch_length_in_hours + leading_hours - 1) * 6 + 1

    data = np.zeros((label_size_0, data_size_1, len(labels.stations), area_size[0], area_size[1]), dtype=np.float32)

    k = 0
    for i in range(label_size_0):
        data[i, ...] = all_data[k:k + data_size_1, ...]
        k = k + (batch_length_in_hours) * 6

    return data


def get_label_batches(from_datetime, to_datetime, leading_hours=2, batch_length_in_hours=6):

    all_labels = get_labels(from_datetime, to_datetime)

    label_size_0 = int((to_datetime - from_datetime).total_seconds() // 3600) // batch_length_in_hours
    data_size_1 = (batch_length_in_hours + leading_hours - 1) * 6 + 1

    lbs = np.ones((label_size_0, data_size_1, len(labels.stations)), dtype=np.float32) * -1

    m = 0
    active_indices = compute_active_indices(leading_hours, batch_length_in_hours)
    for i in range(label_size_0):
        lbs[i, active_indices, ...] = all_labels[m:m + batch_length_in_hours, ...]
        m = m + batch_length_in_hours

    return lbs


# not include to_datetime
def get_batches(from_datetime, to_datetime, leading_hours=2, batch_length_in_hours=6, area_size=(5, 5)):
    data = get_data_batches(from_datetime, to_datetime, leading_hours, batch_length_in_hours, area_size)
    lbs = get_label_batches(from_datetime, to_datetime, leading_hours, batch_length_in_hours)
    return data, lbs


def save_batches(batches, lbs, name="", path=labels.dir_path + "../"):
    np.save(path + "data_train" + name, batches)
    np.save(path + "label_train", lbs)


def load_batches(name="", path=labels.dir_path + "../"):
    data = np.load(path + "data_train" + name + ".npy")
    lbs = np.load(path + "label_train.npy")
    return data, lbs


if __name__ == '__main__':

    global irband
    global dir_path
    parser = argparse.ArgumentParser()
    parser.add_argument("--band", help="band number")
    args = parser.parse_args()
    irband = args.band if args.band is not None else '15'
    dir_path = "/data/irdata/ir" + irband + "nc/"

    print("irband ", irband)

    # f = nc4.Dataset(dir_path + "IR" + irband + "_20170716_1200.nc", 'r')
    # print(f)
    # print(f.variables['wavelength'][0])
    # print(f.variables['times'][0], f.variables['start_time'][0], f.variables['end_time'][0])
    # print(f.variables['latitude'][0], f.variables['latitude'][1], f.variables['latitude'][1390 - 1])
    # print(f.variables['longitude'][0], f.variables['longitude'][1], f.variables['longitude'][834 - 1])
    # print(f.variables['tbb' + irband].shape)

    # plane = nc4_to_nparray(f)
    # np.save("./storm", plane)
    # stations = np.asarray(labels.stations, dtype=np.float32)

    # print(f.variables['latitude'][:])

    # lats = len(f.variables['latitude']) - np.searchsorted(f.variables['latitude'][::-1], stations[:, 0], side="right")
    # longs = np.searchsorted(f.variables['longitude'], stations[:, 1])
    # locs = np.stack([lats, longs], axis=1)

    # for i in range(locs.shape[0]):
    #     cv2.circle(plane, (locs[i][1], locs[i][0]), 2, (0, 0, 0), 2)

    # smaller = cv2.resize(plane, (plane.shape[1] // 2, plane.shape[0] // 2))
    # cv2.imshow("test", smaller[100:540, 50:320])
    # cv2.waitKey()

    # get_data(datetime.datetime(2017, 5, 1, 2, 30), datetime.datetime(2017, 5, 1, 14, 30))
    # get_labels(datetime.datetime(2017, 7, 1, 2, 00), datetime.datetime(2017, 7, 10, 16, 00))

    # regions = sample_atmosphere(plane, locs[0:3], (101, 101))
    # for i in range(regions.shape[0]):
    #     cv2.imshow("test" + str(i), regions[i])
    # cv2.waitKey()

    # print(compute_active_indices(2, 6))
    # data, lbs = get_batches(datetime.datetime(2017, 5, 1, 2, 00), datetime.datetime(2017, 5, 1, 14, 00), batch_length_in_hours=3)

    data, lbs = get_batches(
        datetime.datetime(2017, 9, 1, 0, 0),
        datetime.datetime(2017, 11, 1, 0, 0),
        batch_length_in_hours=6,
        leading_hours=6,
        area_size=(5, 5))
    save_batches(data, lbs, "real_5x5_6_6_" + irband)

    # load_batches()
    # print(data.shape)
    # print(lbs.shape)
    # gen_result_file(lbs, datetime.datetime(2017, 5, 1, 2, 0), datetime.datetime(2017, 10, 1, 00, 00), batch_length_in_hours=6, leading_hours=6)
