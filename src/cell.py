import tensorflow as tf


# input (batch, time, space, depth) -> output (batch, time, space, depth)
class BlockConvolutionCell(tf.contrib.rnn.RNNCell):

    # sizes = (space, depth)
    def __init__(self, sizes, width=30):
        self.sizes_ = sizes

        blocks = sizes[0]
        depth = sizes[1]
        with tf.variable_scope("BCC"):
            self.Wif = tf.Variable(tf.random_normal([width, depth, depth], stddev=0.01), dtype=tf.float32)
            self.Whf = tf.Variable(tf.random_normal([width, depth, depth], stddev=0.01), dtype=tf.float32)
            self.bf = tf.Variable(tf.ones([depth]), dtype=tf.float32)

            self.Wii = tf.Variable(tf.random_normal([width, depth, depth], stddev=0.01), dtype=tf.float32)
            self.Whi = tf.Variable(tf.random_normal([width, depth, depth], stddev=0.01), dtype=tf.float32)
            self.bi = tf.Variable(tf.ones([blocks, depth]), dtype=tf.float32)

            self.Wic = tf.Variable(tf.random_normal([width, depth, depth], stddev=0.01), dtype=tf.float32)
            self.Whc = tf.Variable(tf.random_normal([width, depth, depth], stddev=0.01), dtype=tf.float32)
            self.bc = tf.Variable(tf.zeros([depth]), dtype=tf.float32)

            self.Wio = tf.Variable(tf.random_normal([width, depth, depth], stddev=0.01), dtype=tf.float32)
            self.Who = tf.Variable(tf.random_normal([width, depth, depth], stddev=0.01), dtype=tf.float32)
            self.bo = tf.Variable(tf.zeros([blocks, depth]), dtype=tf.float32)

    @property
    def state_size(self):
        return self.sizes_

    @property
    def output_size(self):
        return self.sizes_

    def init_state(self, batch_size):
        state = tf.random_normal([batch_size, self.sizes_[0], self.sizes_[1]], dtype=tf.float32) * 0.01
        return state

    def conv1d(self, x, k):
        return tf.nn.conv1d(x, k, 1, 'SAME')

    def build_gate(self, h, x, Wi, Wh, b, activation=tf.nn.sigmoid):
        Wix_Whh = self.conv1d(h, Wh) + self.conv1d(x, Wi)
        return activation(Wix_Whh + b)

    def __call__(self, input, state, scope=None):

        gf = self.build_gate(state, input, self.Wif, self.Whf, self.bf)
        gi = self.build_gate(state, input, self.Wii, self.Whi, self.bi)
        C_ = self.build_gate(state, input, self.Wic, self.Whc, self.bc, tf.nn.tanh)
        go = self.build_gate(state, input, self.Wio, self.Who, self.bo)

        C = gf * state + gi * C_
        o = go * tf.nn.tanh(C)

        return o, C


if __name__ == "__main__":
    with tf.Session() as sess:
        cell = BlockConvolutionCell((7, 3))
        state = cell.init_state(20)
        rnn_inputs = tf.random_normal([20, 2, 7 * 3], 0.0, 1.0, dtype=tf.float32)

        for i in range(2):
            output, state = cell(rnn_inputs[:, i, ...], state)

        sess.run(tf.global_variables_initializer())
        out, state = sess.run((output, state), feed_dict={})
        print(out, state)
